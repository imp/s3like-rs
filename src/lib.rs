#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate chrono;
extern crate rocket;

mod s3;

pub use s3::{Bucket, Key, S3Server, StorageProvider};
