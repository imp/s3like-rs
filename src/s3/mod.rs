use chrono;

pub use self::s3server::S3Server;

mod response;
mod s3server;

pub type TimeStamp = chrono::DateTime<chrono::UTC>;

#[derive(Debug)]
pub enum StorageClass {
    Standard,
    StandardIa,
    ReducedRedundancy,
    Glacier,
}

#[derive(Debug)]
pub struct Owner {
    id: String,
    display_name: String,
}

#[derive(Debug)]
pub struct Bucket {
    name: String,
    creation_date: TimeStamp,
}

#[derive(Debug)]
pub struct Key;

pub trait StorageProvider {
    fn name() -> String;
}
