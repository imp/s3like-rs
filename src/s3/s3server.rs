use rocket::Rocket;
use rocket::ignite;
use rocket::response::{Responder, Response, Result};

use super::StorageProvider;

#[derive(Debug)]
struct S3Response;

impl<'r> Responder<'r> for S3Response {
    fn respond(self) -> Result<'r> {
        Ok(Response::new())
    }
}

pub struct S3Server<T> where T: StorageProvider {
    frontend: Rocket,
    provider: T,
}

impl<T> S3Server<T> where T: StorageProvider {
    pub fn new(provider: T) -> Self {
        let frontend = ignite().mount("/", routes![get_service]);
        S3Server {
            frontend: frontend,
            provider: provider,
        }
    }

    pub fn launch(self) {
        self.frontend.launch()
    }
}

#[get("/")]
fn get_service() -> String {
    String::from("one")
}
