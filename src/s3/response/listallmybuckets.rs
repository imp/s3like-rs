use s3::{Owner, Bucket};

#[derive(Debug)]
pub struct ListAllMyBucketsResult {
    owner: Owner,
    buckets: Vec<Bucket>,
}
