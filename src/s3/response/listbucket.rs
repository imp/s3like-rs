use s3::{Key, Owner, StorageClass, TimeStamp};

#[derive(Debug)]
pub struct Contents {
    etag: String,
    owner: Owner,
    key: Key,
    lastmodified: TimeStamp,
    size: usize,
    storageclass: StorageClass,
}

#[derive(Debug)]
pub struct ListBucketResult {
    name: String,
    max_keys: usize,
    prefix: String,
    common_prefixes: String,
    delimeter: String,
    encoding_type: String,
    is_truncated: bool,
    continuation_token: String,
    key_count: usize,
    next_continuation_token: String,
    start_after: String,
}
