extern crate s3like as s3;

use s3::StorageProvider;

#[derive(Debug, Default)]
struct FileProvider;

impl StorageProvider for FileProvider {
    fn name() -> String {
        String::from("FileProvider")
    }
}

#[test]
fn main() {
    s3::S3Server::new(FileProvider::default()).launch();
}
